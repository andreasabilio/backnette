

// Dependencies
var _          = require('underscore');
var Backbone   = require('backbone');
var Marionette = require('marionette'); 

// Clio app components
var Model      = require('./core/Model');
var ajax       = require('./core/_ajax');


// Clio App constructor
module.exports = function(initOps){
    "use strict";

    // Initialize OAuth subsystem
    ajax.init(initOps.oauth);

    var app = new Marionette.Application();
    
    // Register core components without polluting namespace
    return _.extend(app, initOps, Backbone, {Model: Model}, {ajax: ajax.request});
};