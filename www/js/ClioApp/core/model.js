
var _        = require('underscore');
var ajax     = require('./_ajax');
var Backbone = require('backbone');


// Config
var unusedBackboneModelMethods = ['save', 'fetch', 'sync', 'destroy'];

// Helpers
function validateCustomMethodNames(unusedBackboneModelMethods, customMethods) {
    _.map(_.keys(customMethods), function(methodName) {
        if (_.indexOf(unusedBackboneModelMethods, methodName) !== -1 ) {
            throw new Error("We don't use the " + unusedBackboneModelMethods.join(', ') + " model methods, "
                + "so please rename your custom '" + methodName + "' function.");
        }
    });

    return customMethods;
}

function overwriteUnusedModelMethods(unusedBackboneModelMethods, model) {
    _.map(unusedBackboneModelMethods, function(methodName) {
        if (_.indexOf(unusedBackboneModelMethods, methodName) !== -1 ) {
            model[methodName] = function () {
                throw new ReferenceError("We don't use the backbone " + unusedBackboneModelMethods.join(', ') + " model methods. "
                    + "Please set up a custom endpoint for each "
                    + "model method that communicates with the server.");
            }
        }
    }, model);

    return model;
}


// Clio App Model Class definition
module.exports = Backbone.Model.extend({
    
    // Custom constructor
    constructor: function(){
        "use strict";

        // Own model pointer
        var model = this;

        // Validate custom endpoint methods
        var validatedCustomMethods = validateCustomMethodNames(unusedBackboneModelMethods,model.endpoints);

        // Prepare the endpoint model methods
        var methods = _.mapObject(validatedCustomMethods, function(settings){
            return function(params){

                // Set endpoint params
                var endpoint = {
                    url:    model.url + settings.endpoint,
                    method: settings.method,
                    success: function(data, status, jqXHR){
                        // Update the model's data
                        model.set(data);
                    }
                };

                // Insert and call
                return ajax.request(_.extend({}, params || {}, endpoint));
            };
        });

        // Overwrite the backbone model methods that we don't use
        overwriteUnusedModelMethods(unusedBackboneModelMethods, model);

        // Run the parent constructor
        Backbone.Model.apply(_.extend(model, methods), arguments);
    }
});