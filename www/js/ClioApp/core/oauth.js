
module.exports = {

    // Properties
    token:      null,
    ttl:        null,
    refreshing: false,
    endpoint:   'http://localhost:3000/api/auth/token',


    // Initialize the oauth subsystem
    init: function(params){
        "use strict";

        // Set initial data
        oauth.token = params.token;
        oauth.ttl   = oauth.calculateTTL(params.ttl);

        // Start token refresh timer
        setInterval(oauth.refresh, oauth.ttl);
    },

    // Provide an OAuth token via callback
    getToken: function(){
        "use strict";

        return oauth.token;
    },

    // Refresh the OAuth token
    // This runs shortly before token needs to be refreshed
    refresh: function(){
        "use strict";

        // Create new deferred
        var deferred = $.Deferred();

        // Request options
        var options = {
            url: oauth.endpoint,
            success: function(data){

                deferred.resolve(data.token);
                oauth.ttl = oauth.calculateTTL(data.ttl);
            },
            error: function(jqHXR, textStatus, error){
                deferred.reject(error);
                oauth.refresh();
            }
        };

        // Fetch new token
        $.ajax(options);

        // Set token as promise
        oauth.token = deferred.promise();
    },

    calculateTTL: function(ttl){
        "use strict";

        // Renew the token when 10% of ttl remains
        return (ttl - ((ttl * 10)/100)) * 1000;
    }
};